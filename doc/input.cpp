




/*
    Raw input from OS.
    Input mapping and dispatch to current handles.
    High lvl handler code?



    Context         A context defines what inputs are available for the player at a given time.
        Actions     Single-time thing, like casting a spell or open a door.
        States      Continuous activities, like running, shooting or scrolling menus.
        Ranges      Range can have a number value associated with it, normalized spans (0.0 to 1.0 or -1 to 1). Most useful for supporting analog inputs.
        Each context needs to have its own input map, to handle different situations.
        There is a one-to-one relationship between contexts and input maps, so its good to implement them as a single class or group of functions.

    Mapping         From a raw input to an action, state or range.
        Its a c++ map object (table, dictionary).
        Each context defines an input map.
        To take an identified type of hardware input and convert it to final type of input.

    Dispatching
        Two options, callbacks or polling.
        Callbacking, every time some input occurs, we call special functions which handle the input.
        Polling, code is responsible to ask the input management each frame for what inputs are occuring and react occordingly.

    Processing every frame:
        Raw input is obtained from OS.
        The currently active contexts are evaluated. Input mapping is performed.
        Once a list of actions, states and ranges is obtained. We pack this up into a special data structure and invoke the appropriate callsbacks.


    More than one context should be active at once, for example one for basic things like running around and one for the tool the player is holding.
    Maybe implement this as a simple ordered list:
        each context in the list is given raw input for this frame, 
        if the first context can validly map that raw input to an action, state or range,
        it does so, otherwise, pass it on to next context in list.
        This allows us to prioritize contexts.
        Example of order: gui, controlling entity, holding entity, interacting entity.


    Example: in context A, key Q corresponds to action 7.


    key A --- 




*/






Mapper mapper;
mapper.push(mainContext);
mapper.push(playerContext);
mapper.push(toolContext);
mapper.push(machineContext);






enum Action
{
    TEST_ACTION,
    BAJS_ACTION,
    FITTA_ACTION
};

enum State
{
    KISS_STATE,
    KUK_STATE
};

enum Range
{
    SOME_RANGE,
    OTHER_RANGE
};


class InputContext
{
public:
    InputContext(){}



    bool MapButtonToState(RawInputButton, State& out) {}
    bool MapAxisToRange(RawInputAxis axis, Range& out) {}
    double getSensitivity(Range range) {}
    
private:
    std::unordered_map<RawInputButton, Action> actionMap;
    std::unordered_map<RawInputButton, State> stateMap;
    std::unordered_map<RawInputAxis, range> rangeMap;

    std::unordered_map<Range, double> sensitivityMap;
};
















struct BajsComponent
{
    std::unordered_map<RawInputButton, Action> actionMap;
    std::unordered_map<RawInputButton, State> stateMap;
    std::unordered_map<RawInputAxis, range> rangeMap;
};











